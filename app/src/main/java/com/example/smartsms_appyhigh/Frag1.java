package com.example.smartsms_appyhigh;


import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.telephony.SmsMessage;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag1 extends Fragment {
   // private static final String TAG = MySmsReceiver.class.getSimpleName();
    public static final String pdu_type = "pdus";

    DBHandler db;



    public Frag1() {
        // Required empty public constructor
    }

  //  private static SmsActivity inst;
    ArrayList<String> smsMessagesList = new ArrayList<String>();
    ListView smsListView;
    ArrayAdapter arrayAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_frag1, container, false);
         smsListView=view.findViewById(R.id.SMSList);

        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, smsMessagesList);
        smsListView.setAdapter(arrayAdapter);


        assert getArguments() != null;
        String value="";
            MainActivity activity=(MainActivity)getActivity();
            String name=activity.getname();
            String msg=activity.getmsg();
            String data=name+"\n"+msg;
            Log.e("screen","tag value = "+data);
            arrayAdapter.clear();
            //arrayAdapter.add(data);

        db=new DBHandler(getContext());

        db.insertRecord(data);
        //Toast.makeText(getActivity(), "record inserted", Toast.LENGTH_LONG).show();

        Log.e("SQLITEDATA","saveddata = "+db.getRecords());
       // Toast.makeText(activity, "saved data = "+db.getRecords(), Toast.LENGTH_SHORT).show();

        arrayAdapter.add(db.getRecords());
  return view;
    }

 /*   public void updateList(final String smsMessage) {
        arrayAdapter.insert(smsMessage, 0);
        arrayAdapter.notifyDataSetChanged();
    }*/

}
