package com.example.smartsms_appyhigh;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHandler extends SQLiteOpenHelper {

    private static final String DB_NAME="database";
    private static final int DB_VERSION=1;
    private static final String TABLE_NAME="data";
    private static final String ID_COL="id";
    private static final String NAME_COL="name";
  //  private static final String MESSAGE="msg";


   public DBHandler(Context context){
       super(context,DB_NAME,null,DB_VERSION);
   }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query="CREATE TABLE "+TABLE_NAME+" " +
                "("+ID_COL+" INTEGER PRIMARY KEY AUTOINCREMENT,"+NAME_COL+" TEXT)";

        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
    public void insertRecord(String msg){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(NAME_COL,msg);
       // values.put(MESSAGE,message);
        db.insert(TABLE_NAME,null,values);
        db.close();
    }
    public String getRecords(){
        String query="SELECT * FROM "+TABLE_NAME;
        String result="";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(query,null);
        cursor.moveToFirst();
        while(cursor.isAfterLast()==false){
            result+=cursor.getString(0)+" "+cursor.getString(1)+"\n";
            cursor.moveToNext();
        }
        db.close();
        return result;
    }
}
