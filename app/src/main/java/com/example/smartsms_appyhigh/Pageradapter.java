package com.example.smartsms_appyhigh;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class Pageradapter extends FragmentStatePagerAdapter {
    int nooftab;

    public Pageradapter(FragmentManager fm,int nooftab) {
        super(fm);
        this.nooftab=nooftab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Frag1 frag1=new Frag1();
                return frag1;
            case 1:
                Frag2 frag2=new Frag2();
                return frag2;
            case 2:
                Frag3 frag3=new Frag3();
                return frag3;
                default:
                    return null;

        }

    }

    @Override
    public int getCount() {
        return nooftab;
    }
}
