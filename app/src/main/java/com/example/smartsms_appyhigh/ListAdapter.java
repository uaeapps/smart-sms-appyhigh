package com.example.smartsms_appyhigh;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class ListAdapter extends ArrayAdapter <MessageData>{
    List<MessageData> list;

    //activity context
    Context context;

    //the layout resource file for the list items
    int resource;

    public ListAdapter(Context context, int resource, List<MessageData> list) {
        super(context, resource, list);
        this.context = context;
        this.resource = resource;
        this.list = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        //getting the view
        View view = layoutInflater.inflate(resource, null, false);

        //getting the view elements of the list from the view
       // ImageView imageView = view.findViewById(R.id.imageView);
        TextView sendername = view.findViewById(R.id.sendername);
        TextView sendermsg = view.findViewById(R.id.sendermsg);
        Button buttonDelete = view.findViewById(R.id.btndelete);

        //getting the hero of the specified position
        MessageData msgdata = list.get(position);
        MainActivity activity=(MainActivity)getContext();
        String name=activity.getname();
        String msg=activity.getmsg();
        //adding values to the list item

      //  imageView.setImageDrawable(context.getResources().getDrawable(hero.getImage()));
       sendername.setText(msgdata.getSendername());
        sendermsg.setText(msgdata.getSendermsg());
        Log.e("Class","name = "+sendername+"sender msg = "+sendermsg);
        Toast.makeText(activity, "msg= "+sendermsg+"name = "+sendername, Toast.LENGTH_SHORT).show();




       return view;

    }
}
