package com.example.smartsms_appyhigh;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag3 extends Fragment {
    DBHandler db;
    ArrayList<String> smsMessagesList = new ArrayList<String>();
    ListView smsListView;
    ArrayAdapter arrayAdapter;

    public Frag3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_frag3, container, false);
        smsListView=view.findViewById(R.id.SMSList);

        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, smsMessagesList);
        smsListView.setAdapter(arrayAdapter);


        assert getArguments() != null;
        String value="";
        MainActivity activity=(MainActivity)getActivity();
        String name=activity.getname();
        String msg=activity.getmsg();
        String data=name+"\n"+msg;
        Log.e("screen","tag value = "+data);
        arrayAdapter.clear();
        //arrayAdapter.add(data);

        db=new DBHandler(getContext());

        db.insertRecord(data);
       // Toast.makeText(getActivity(), "record inserted", Toast.LENGTH_LONG).show();

        Log.e("SQLITEDATA","saveddata = "+db.getRecords());
        // Toast.makeText(activity, "saved data = "+db.getRecords(), Toast.LENGTH_SHORT).show();
//if (db.getRecords().contains(var))
        arrayAdapter.add(db.getRecords());

smsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
       /* try {
            String[] smsMessages = smsMessagesList.get(i).split("\n");
            String address = smsMessages[0];
            String smsMessage = "";
            for (int u = 1; i < smsMessages.length; ++i) {
                smsMessage += smsMessages[i];
            }

            String smsMessageStr = address + "\n";
            smsMessageStr += smsMessage;
            Toast.makeText(getContext(), smsMessageStr, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }
});
return view;
    }

}
