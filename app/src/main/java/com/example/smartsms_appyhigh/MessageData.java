package com.example.smartsms_appyhigh;

public class MessageData {
    String sendername;
    String sendermsg;


    public MessageData(String sendername, String sendermsg) {
        this.sendername = sendername;
        this.sendermsg = sendermsg;
    }

    public String getSendername() {
        return sendername;
    }

    public String getSendermsg() {
        return sendermsg;
    }
}
