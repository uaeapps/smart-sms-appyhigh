package com.example.smartsms_appyhigh;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SendSms extends AppCompatActivity {
EditText edtphno,edtmsg;
Button btnsend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);
        edtphno=findViewById(R.id.edtphno);
        edtmsg=findViewById(R.id.edtmsg);
        btnsend=findViewById(R.id.btnsend);
       btnsend.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String phoneNo = edtphno.getText().toString();
               String sms = edtmsg.getText().toString();

               try {
                   SmsManager smsManager = SmsManager.getDefault();
                   smsManager.sendTextMessage(phoneNo, null, sms, null, null);
                   Toast.makeText(getApplicationContext(), "SMS Sent!",
                           Toast.LENGTH_LONG).show();
               } catch (Exception e) {
                   Toast.makeText(getApplicationContext(),
                           "SMS faild, please try again later!"+e,
                           Toast.LENGTH_LONG).show();
                   e.printStackTrace();
               }


           }
       });
    }
}
