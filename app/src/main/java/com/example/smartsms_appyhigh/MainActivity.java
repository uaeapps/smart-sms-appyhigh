package com.example.smartsms_appyhigh;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static MainActivity inst;
    ArrayList<String> smsMessagesList = new ArrayList<String>();
    ListView smsListView;
    String name,msg;
    String str;
    ArrayAdapter arrayAdapter;

    public static MainActivity instance() {
        return inst;
    }
    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TabLayout tabLayout=findViewById(R.id.tablayout);
        fab=findViewById(R.id.fab);

        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, smsMessagesList);
        Log.e("error","error = "+arrayAdapter);
        tabLayout.addTab(tabLayout.newTab().setText("All Sms"));
        tabLayout.addTab(tabLayout.newTab().setText("Spam"));
        tabLayout.addTab(tabLayout.newTab().setText("Advertising"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        if(ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") == PackageManager.PERMISSION_GRANTED)
        { refreshSmsInbox();}
        else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{"android.permission.READ_SMS"},123);
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this,SendSms.class);
                startActivity(i);

               // Toast.makeText(MainActivity.this, "Message to be sent", Toast.LENGTH_SHORT).show();
            }
        });
        final ViewPager viewPager=findViewById(R.id.viewpager);
        Pageradapter pageradapter=new Pageradapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(pageradapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });





    }

    private void refreshSmsInbox() {
        ContentResolver contentResolver = getContentResolver();
            try (Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null)) {


                int indexBody = smsInboxCursor.getColumnIndex("body");
                int indexAddress = smsInboxCursor.getColumnIndex("address");
                if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
                arrayAdapter.clear();
                do {
                    name=smsInboxCursor.getString(indexAddress);
                    // str = "SMS From: " + smsInboxCursor.getString(indexAddress) +
                       //     "\n" + smsInboxCursor.getString(indexBody) + "\n";
                    msg=smsInboxCursor.getString(indexBody);
                   // Toast.makeText(inst, "Message = " + str, Toast.LENGTH_SHORT).show();
                   // Log.e("msg", "message = " + smsInboxCursor.getString(indexAddress)+"\n"+smsInboxCursor.getString(indexBody) );

                    arrayAdapter.add(str);

                    //Bundle bundle=new Bundle();
                   // bundle.putString("text",str);
                    //Frag1 frag1=new Frag1();
                    //frag1.setArguments(bundle);
                  //  Log.e("str","bundle = "+bundle);

                } while (smsInboxCursor.moveToNext());
            }
            catch (Exception e){
                Log.e("error","error due to this method = "+e);
            }
        }

    public void updateList(final String smsMessage) {
        arrayAdapter.insert(smsMessage, 0);
        arrayAdapter.notifyDataSetChanged();
    }
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        try {
            String[] smsMessages = smsMessagesList.get(pos).split("\n");
            String address = smsMessages[0];
            String smsMessage = "";
            for (int i = 1; i < smsMessages.length; ++i) {
                smsMessage += smsMessages[i];
            }

            String smsMessageStr = address + "\n";
            smsMessageStr += smsMessage;
            Toast.makeText(this, smsMessageStr, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
public String getname(){
        return name;
}
public String getmsg(){
        return msg;
}
}
