package com.example.smartsms_appyhigh;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag2 extends Fragment  {
   ListView smsListView;
List <MessageData> list;
    DBHandler db;

    //
    ArrayList<String> smsMessagesList = new ArrayList<String>();

    ArrayAdapter arrayAdapter;

    public Frag2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_frag2, container, false);
        smsListView=view.findViewById(R.id.SMSList);

        arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, smsMessagesList);
        smsListView.setAdapter(arrayAdapter);


        assert getArguments() != null;
        String value="";
        MainActivity activity=(MainActivity)getActivity();
        String name=activity.getname();
        String msg=activity.getmsg();
        String data=name+"\n"+msg;
        Log.e("screen","tag value = "+data);
        arrayAdapter.clear();
        //arrayAdapter.add(data);

        db=new DBHandler(getContext());

        db.insertRecord(data);
        //Toast.makeText(getActivity(), "record inserted", Toast.LENGTH_LONG).show();

        Log.e("SQLITEDATA","saveddata = "+db.getRecords());
        // Toast.makeText(activity, "saved data = "+db.getRecords(), Toast.LENGTH_SHORT).show();
//if (db.getRecords().contains(var))
        arrayAdapter.add(db.getRecords());


      /*
        assert getArguments() != null;
        String value="";
        MainActivity activity=(MainActivity)getActivity();
        String name=activity.getname();
        String msg=activity.getmsg();
        String data=name+"\n"+msg;
        Log.e("screen","tag value = "+data);
        db=new DBHandler(getContext());

        db.insertRecord(data);
        //Toast.makeText(getActivity(), "record inserted", Toast.LENGTH_LONG).show();

        Log.e("SQLITEDATA","saveddata = "+db.getRecords());
       // String result=db.getRecords();
        // Toast.makeText(activity, "saved data = "+db.getRecords(), Toast.LENGTH_SHORT).show();

       // arrayAdapter.add(db.getRecords());
list=new ArrayList<>();
        smsListView=view.findViewById(R.id.SMSList);
        ListAdapter adapter=new ListAdapter(getContext(),R.layout.msg_layout,list);
        smsListView.setAdapter(adapter);
    //list.add(result);
*/
   return view;
    }

}
